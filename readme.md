# Community Tests für die sechste Hausübung der AuD 2021


Zum Ausführen der Tests sollte eine [eigene JUnit Run Configuration](https://git.rwth-aachen.de/groups/aud-tests/-/wikis/JUnit-Run-Configuration) angelegt werden, da durch den gradle task nicht alle Meldungen angezeigt werden (z.B. warum Tests ignoriert werden).

Einige Methoden müssen intern nach bestimmten Anforderungen aufgebaut sein. Mit Black-box testing (worunter diese Tests fallen) kann man dabei allerdings nur bedingt Aussagen über die Korrektheit treffen, weil die Ergebnisse identisch sind. \
Die Tests können also nur prüfen, ob die Definitionen von Klassen, Attributen und Methoden sowie deren Werte bzw. Rückgaben korrekt sind. Da Attribute bzw. Methoden nicht immer mit Namen benannt sind, müssen sie manchmal nach Kriterien bestimmt werden, wodurch es zu einer `NoSuchFieldException` bzw. `NoSuchMethodException` (auch für Konstruktoren) kommen kann. Details, z.B. wo der Fehler aufgetreten ist, finden sich im Stacktrace und im jeweiligen Prädikat.

Einige Aspekte der Tests können mithilfe der Datei [`Config.java`](src/test/java/h06/util/Config.java) angepasst werden. Mehr Infos finden sich in der Datei selbst.

Die Tests kommen mit einem Installer sowie einem Update-Mechanismus, der vor jeder Ausführung nach Updates sucht. Da das einige Sekunden in Anspruch nehmen kann und vielleicht auch anderweitig nicht gewünscht ist, kann diese Funktionalität ausgeschaltet werden. Wird der Installer oder der Update-Mechanismus verwendet, muss das Arbeitsverzeichnis gleich dem Projektordner sein. Das Verhalten kann mit Änderung der Konstanten `CHECK_FOR_UPDATES`, `CHECK_HASHES` und `AUTO_UPDATE` in der Konfiguration verändert werden. \
Vielen Dank an @svenja.kernig für die Idee und Hilfe mit XML :D

Am [Ende](#abhngigkeiten-der-tests-voneinander) des Readmes ist ein Diagramm, in dem die Abhängigkeiten der Tests eingetragen sind. Diese sind nicht bindend, d.h. Tests könnten auch ausgeführt werden, sollten die Tests in der vorausgesetzten Klasse nicht komplett durchlaufen. Es wäre aber natürlich besser Fehler upstream zu beheben.

DISCLAIMER: Das Durchlaufen der Tests ist keine Garantie dafür, dass die Aufgaben vollständig korrekt implementiert sind.

<br>

## h06.hashFunctions.OtherToIntFunctionTest

### .testDefinition()
Überprüft, ob die Definition des Interfaces `h06.hashFunctions.OtherToIntFunction` korrekt ist:
- Klasse bzw. Interface selbst
  - es handelt sich um ein Interface
  - ist public
  - ist generisch und hat einen Typparameter `T`
- Methoden
  - `apply(T)`
    - ist abstrakt
    - hat den Rückgabetyp `int`
  - `getTableSize()`
    - ist abstrakt
    - hat den Rückgabetyp `int`
  - `setTableSize(int)`
    - ist abstrakt
    - hat den Rückgabetyp `void`

## h06.hashFunctions.OtherAndIntToIntFunctionTest

### .testDefinition()
Überprüft, ob die Definition des Interfaces `h06.hashFunctions.OtherAndIntToIntFunction` korrekt ist:
- Klasse bzw. Interface selbst
  - es handelt sich um ein Interface
  - ist public
  - ist generisch und hat einen Typparameter `T`
- Methoden
  - `apply(T, int)`
    - ist abstrakt
    - hat den Rückgabetyp `int`
  - `getTableSize()`
    - ist abstrakt
    - hat den Rückgabetyp `int`
  - `setTableSize(int)`
    - ist abstrakt
    - hat den Rückgabetyp `void`

## h06.hashFunctions.HashCodeTableIndexFctTest

Voraussetzungen:
- `h06.hashFunction.OtherToIntFunctionTest#testDefinition()`

### .testDefinition()
Überprüft, ob die Definition der Klasse `h06.hashFunctions.HashCodeTableIndexFct` korrekt ist:
- Klasse bzw. Interface selbst
  - es handelt sich um eine Klasse
  - ist public
  - ist nicht abstract
  - ist generisch und hat einen Typparameter `T`
  - implementiert das Interface `h06.hashFunctions.OtherToIntFunction<T>`
- Konstruktor
  - ist public

### .testInstance()
Überprüft, ob eine Instanz der Klasse `h06.hashFunctions.HashCodeTableIndexFct` korrekt ist:
- ein mit zufälliger Tabellengröße (0-99) und offset = 2 instanziiertes Objekt setzt die jeweiligen Attribute korrekt

### .testApply(int, int, java.lang.Object)
Parametrisierter Test. Überprüft, ob die Methode `h06.hashFunctions.HashCodeTableIndexFct#apply(T)` korrekt funktioniert:
- die Methode liefert für gegebene Parameter den richtigen Wert zurück

### .testGetTableSize(int)
Parametrisierter Test. Überprüft, ob die Methode `h06.hashFunctions.HashCodeTableIndexFct#getTableSize()` korrekt funktioniert:
- die Methode liefert den korrekten Wert für die Tabellengröße zurück

### .testSetTableSize(int)
Parametrisierter Test. Überprüft, ob die Methode `h06.hashFunctions.HashCodeTableIndexFct#setTableSize(int)` korrekt funktioniert:
- die Methode setzt den korrekten Wert für die Tabellengröße

## h06.hashFunctions.LinearProbingTableIndexFctTest

Voraussetzungen:
- `h06.hashFunction.OtherToIntFunctionTest#testDefinition()`
- `h06.hashFunction.OtherAndIntToIntFunctionTest#testDefinition()`

### .testDefinition()
Überprüft, ob die Definition der Klasse `h06.hashFunctions.LinearProbingTableIndexFct` korrekt ist:
- Klasse bzw. Interface selbst
  - es handelt sich um eine Klasse
  - ist public
  - ist nicht abstract
  - ist generisch und hat einen Typparameter `T`
  - implementiert das Interface `h06.hashFunctions.OtherAndIntToIntFunction<T>`
- Konstruktor
  - ist public

### .testInstance()
Überprüft, ob eine Instanz der Klasse `h06.hashFunctions.LinearProbingTableIndexFct` korrekt ist:
- ein mit einer Proxy-Instanz instanziiertes Objekt setzt das jeweilige Attribut korrekt

### .testApply(int, int, java.lang.Object)
Parametrisierter Test. Überprüft, ob die Methode `h06.hashFunctions.LinearProbingTableIndexFct#apply(T, int)` korrekt funktioniert:
- die Methode liefert für gegebene Parameter den richtigen Wert zurück

### .testGetTableSize(int)
Parametrisierter Test. Überprüft, ob die Methode `h06.hashFunctions.LinearProbingTableIndexFct#getTableSize()` korrekt funktioniert:
- die Methode liefert den korrekten Wert für die Tabellengröße zurück

### .testSetTableSize(int)
Parametrisierter Test. Überprüft, ob die Methode `h06.hashFunctions.LinearProbingTableIndexFct#setTableSize(int)` korrekt funktioniert:
- die Methode setzt den korrekten Wert für die Tabellengröße

## h06.hashFunctions.DoubleHashingTableIndexFctTest

Voraussetzungen:
- `h06.hashFunction.OtherToIntFunctionTest#testDefinition()`
- `h06.hashFunction.OtherAndIntToIntFunctionTest#testDefinition()`
- `h06.hashFunction.HashCodeTableIndexFctTest#testDefinition()`

### .testDefinition()
Überprüft, ob die Definition der Klasse `h06.hashFunctions.DoubleHashingTableIndexFct` korrekt ist:
- Klasse bzw. Interface selbst
  - es handelt sich um eine Klasse
  - ist public
  - ist nicht abstract
  - ist generisch und hat einen Typparameter `T`
  - implementiert das Interface `h06.hashFunctions.OtherAndIntToIntFunction<T>`
- Konstruktor
  - ist public
- Attribute
  - `fct1`
    - ist private und final
    - ist nicht static
  - `fct1`
    - ist private und final
    - ist nicht static

### .testInstance()
Überprüft, ob eine Instanz der Klasse `h06.hashFunctions.DoubleHashingTableIndexFct` korrekt ist:
- ein mit einer HashCodeTableIndexFct-Instanzen instanziiertes Objekt setzt das jeweilige Attribut korrekt

### .testApply(int, int, java.lang.Object)
Parametrisierter Test. Überprüft, ob die Methode `h06.hashFunctions.DoubleHashingTableIndexFct#apply(T, int)` korrekt funktioniert:
- die Methode liefert für gegebene Parameter den richtigen Wert zurück

### .testGetTableSize(int)
Parametrisierter Test. Überprüft, ob die Methode `h06.hashFunctions.DoubleHashingTableIndexFct#getTableSize()` korrekt funktioniert:
- die Methode liefert den korrekten Wert für die Tabellengröße zurück

### .testSetTableSize(int)
Parametrisierter Test. Überprüft, ob die Methode `h06.hashFunctions.DoubleHashingTableIndexFct#setTableSize(int)` korrekt funktioniert:
- die Methode setzt den korrekten Wert für die Tabellengröße

## h06.hashFunctions.MyDateTest

### .testDefinition()
Überprüft, ob die Definition der Klasse `h06.hashFunctions.MyDate` korrekt ist:
- Klasse bzw. Interface selbst
  - ist public
- Konstruktor
  - ist public
- Attribute
  - hat mindestens 5 Konstanten vom Typ `int` oder mindestens eine Konstante vom Typ `int[]`
  - hat mindestens eine Konstante vom Typ `boolean`
  - hat mindestens 5 Konstanten vom Typ `long` oder mindestens eine Konstante vom Typ `long[]`
- Methoden
  - `getYear()`
    - hat den Rückgabetyp `int`
  - `getMonth()`
    - hat den Rückgabetyp `int`
  - `getDay()`
    - hat den Rückgabetyp `int`
  - `getHour()`
    - hat den Rückgabetyp `int`
  - `getMinute()`
    - hat den Rückgabetyp `int`
  - `hashCode()`
    - ist public
    - hat den Rückgabetyp `int`

### .testInstance()
Überprüft, ob eine Instanz der Klasse `h06.hashFunctions.MyDate` korrekt ist:
- die richtigen Hashes werden zurückgegeben, bei Aufruf von `hashCode()` (01.01.1970 00:00 und 31.12.2021 23:59, jeweils mit `true` und `false`)

## h06.hashTables.MyMapTest

### .testDefinition()
Überprüft, ob die Definition des Interfaces `h06.hashFunctions.MyMap` korrekt ist:
- Klasse bzw. Interface selbst
  - es handelt sich um ein Interface
  - ist public
  - ist generisch und hat zwei Typparameter `K` und `V`
- Methoden
  - `contains(K)`
    - ist abstrakt
    - hat den Rückgabetyp `boolean`
  - `getValue(K)`
    - ist abstrakt
    - hat den Rückgabetyp `V`
  - `put(K, V)`
    - ist abstrakt
    - hat den Rückgabetyp `V`
  - `remove(K)`
    - ist abstrakt
    - hat den Rückgabetyp `V`

## h06.hashTables.MyIndexHoppingHashMapTest

Voraussetzungen:
- `h06.hashFunction.OtherAndIntToIntFunctionTest#testDefinition()`
- `h06.hashTable.MyMapTest#testDefinition()`

### .testDefinition()
Überprüft, ob die Definition der Klasse `h06.hashFunctions.MyIndexHoppingHashMap` korrekt ist:
- Klasse bzw. Interface selbst
  - es handelt sich um eine Klasse
  - ist public
  - ist generisch und hat zwei Typparameter `K` und `V`
  - implementiert das Interface `h06.hashTables.MyMap<K, V>`
- Konstruktor
  - ist public
- Attribute
  - `theKeys`
    - ist private
    - ist nicht static oder final
    - hat den statischen Typen `K[]`
  - `theValues`
    - ist private
    - ist nicht static oder final
    - hat den statischen Typen `V[]`
  - `occupiedSinceLastRehash`
    - ist private
    - ist nicht static oder final
    - hat den statischen Typen `boolean[]`
- Methoden
  - `rehash()`
    - ist private
    - ist nicht static

### .testInstance()
Überprüft, ob eine Instanz der Klasse `h06.hashTables.MyIndexHoppingHashMap` korrekt ist:
- ein mit 10, 5, 0.5 und einer Proxy-Instanz instanziiertes Objekt setzt die jeweiligen Attribute korrekt
- `theKeys`, `theValues` und `occupiedSinceLastRehash` haben die korrekte Größe

### .testContainsKey(java.util.List<java.lang.Object>)
Parametrisierter Test. Hängt von `h06.hashTables.MyIndexHoppingHashMap#put(K, V)` ab. Überprüft, ob die Methode `h06.hashTables.MyIndexHoppingHashMap#contains(K)` korrekt funktioniert:
- für eingefügte Elemente wird `true` zurückgegeben
- für nicht eingefügte Elemente wird `false` zurückgegeben
- die Methode liefert konsistente Ergebnisse

### .testGetValue(java.util.List<java.lang.Object>)
Parametrisierter Test. Hängt von `h06.hashTables.MyIndexHoppingHashMap#put(K, V)` ab. Überprüft, ob die Methode `h06.hashTables.MyIndexHoppingHashMap#getValue(K)` korrekt funktioniert:
- für eingefügte Elemente wird dieses Element zurückgegeben
- für nicht eingefügte Elemente wird `null` zurückgegeben
- die Methode liefert konsistente Ergebnisse

### .testPut(java.util.List<java.lang.Object>)
Parametrisierter Test. Überprüft, ob die Methode `h06.hashTables.MyIndexHoppingHashMap#put(K, V)` korrekt funktioniert:
- gegebene Elemente werden eingefügt und für jedes Element wird `null` zurückgegeben
- die Arrays werden beim Übersteigen des Thresholds korrekt vergrößert
- die gegebenen Elemente werden erneut eingefügt und für jedes Element wird dasselbe Element zurückgegeben

### .testRemove(java.util.List<java.lang.Object>)
Parametrisierter Test. Hängt von `h06.hashTables.MyIndexHoppingHashMap#put(K, V)` ab. Überprüft, ob die Methode `h06.hashTables.MyIndexHoppingHashMap#remove(K)` korrekt funktioniert:
- für eingefügte Elemente wird dieses Element zurückgegeben
- das Element kommt nicht mehr in `theKeys` oder `theValues` vor
- für nicht eingefügte Elemente wird `null` zurückgegeben
- die Methode liefert konsistente Ergebnisse

### .testRehash(java.util.List<java.lang.Object>)
Parametrisierter Test. Überprüft, ob die Methode `h06.hashTables.MyIndexHoppingHashMap#remove(K)` korrekt funktioniert:
- die Arrays werden beim manuellen Aufruf der Methode korrekt vergrößert
- die Arrays enthalten noch die vorher eingefügten Elemente
- `occupiedSinceLastRehash` enthält die gleiche Anzahl an Werten, die `true` sind

## h06.hashTables.KeyValuePairTest

### .testDefinition()
Überprüft, ob die Definition der Klasse `h06.hashFunctions.KeyValuePair` korrekt ist:
- Klasse bzw. Interface selbst
  - es handelt sich um eine nicht abstrakte Klasse
  - ist public
  - ist generisch und hat zwei Typparameter `K` und `V`
- Methoden
  - `getKey()`
    - ist public
    - hat den Rückgabetyp `K`
  - `getValue()`
    - ist public
    - hat den Rückgabetyp `V`
  - `setValue(V)`
    - ist public

### .testInstance()
Überprüft, ob eine Instanz der Klasse `h06.hashTables.KeyValuePair` korrekt ist:
- ein mit "key" und "value" instanziiertes Objekt setzt die jeweiligen Attribute korrekt

### .testGetKey(java.lang.Object, java.lang.Object)
Parametrisierter Test. Überprüft, ob die Methode `h06.hashTables.KeyValuePair#getKey()` korrekt funktioniert:
- die Methode liefert für gegebene Parameter den richtigen Wert zurück

### .testGetValue(java.lang.Object, java.lang.Object)
Parametrisierter Test. Überprüft, ob die Methode `h06.hashTables.KeyValuePair#getValue()` korrekt funktioniert:
- die Methode liefert für gegebene Parameter den richtigen Wert zurück

### .testSetValue(java.lang.Object, java.lang.Object)
Parametrisierter Test. Überprüft, ob die Methode `h06.hashTables.KeyValuePair#setValue(V)` korrekt funktioniert:
- die Methode setzt für gegebene Parameter den richtigen Wert

## h06.hashTables.MyListsHashMapTest

Voraussetzungen:
- `h06.hashFunction.OtherToIntFunctionTest#testDefinition()`
- `h06.hashTables.MyMapTest#testDefinition()`
- `h06.hashTables.KeyValuePairTest#testDefinition()`

### .testDefinition()
Überprüft, ob die Definition der Klasse `h06.hashFunctions.MyListsHashMap` korrekt ist:
- Klasse bzw. Interface selbst
  - es handelt sich um eine Klasse
  - ist public
  - ist nicht abstrakt
  - ist generisch und hat zwei Typparameter `K` und `V`
  - implementiert das Interface `h06.hashTables.MyMap<K, V>`
- Konstruktor
  - ist public

### .testInstance()
Überprüft, ob eine Instanz der Klasse `h06.hashTables.MyListsHashMap` korrekt ist:
- ein mit einer Proxy-Instanz instanziiertes Objekt setzt die jeweiligen Attribute korrekt
- die Komponenten von `table` sind Instanzen von `java.util.LinkedList`

### .testContainsKey(java.util.List<java.lang.Object>)
Parametrisierter Test. Hängt von `h06.hashTables.MyListsHashMap#put(K, V)` ab. Überprüft, ob die Methode `h06.hashTables.MyListsHashMap#contains(K)` korrekt funktioniert:
- für eingefügte Elemente wird `true` zurückgegeben
- für nicht eingefügte Elemente wird `false` zurückgegeben

### .testGetValue(java.util.List<java.lang.Object>)
Parametrisierter Test. Hängt von `h06.hashTables.MyListsHashMap#put(K, V)` ab. Überprüft, ob die Methode `h06.hashTables.MyListsHashMap#getValue(K)` korrekt funktioniert:
- für eingefügte Elemente wird dieses Element zurückgegeben
- für nicht eingefügte Elemente wird `null` zurückgegeben

### .testPut(java.util.List<java.lang.Object>)
Parametrisierter Test. Überprüft, ob die Methode `h06.hashTables.MyListsHashMap#put(K, V)` korrekt funktioniert:
- gegebene Elemente werden eingefügt und für jedes Element wird `null` zurückgegeben
- die Liste, in die das Element eingefügt werden sollte, hat die richtige Länge
- die Liste, in die das Element eingefügt werden sollte, hat das Element an Index 0
- die gegebenen Elemente werden erneut eingefügt und für jedes Element wird dasselbe Element zurückgegeben

### .testRemove(java.util.List<java.lang.Object>)
Parametrisierter Test. Hängt von `h06.hashTables.MyListsHashMap#put(K, V)` ab. Überprüft, ob die Methode `h06.hashTables.MyListsHashMap#remove(K)` korrekt funktioniert:
- für eingefügte Elemente wird dieses Element zurückgegeben
- das Element kommt nicht mehr in `theKeys` oder `theValues` vor
- für nicht eingefügte Elemente wird `null` zurückgegeben

## h06.test.RuntimeTestTest

### .testDefinition()
Überprüft, ob die Definition der Klasse `h06.test.RuntimeTest` korrekt ist:
- Konstruktor
  - ist public
- Methoden
  - `Test(int, int, int, int)`
    - die Methode ist public


## Abhängigkeiten der Tests voneinander

![Abhängigkeiten der Tests](https://i.imgur.com/4fbhqAo.png)
