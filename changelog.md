### Version 1.0.1
- [util/Config.java](src/test/java/h06/util/Config.java)
  - Removed Updater.java from list of excluded files
- [hashTables/MyIndexHoppingHashMap.java](src/test/java/h06/hashTables/MyIndexHoppingHashMapTest.java)
  - Removed tests for "initial table size" field
  - Replaced building arrays with calls to `put(K, V)` (Many thanks to Nick Hammerbacher)
- [hashTable/MyListsHashMapTest.java](src/test/java/h06/hashTables/MyListsHashMapTest.java)
  - Replaced building lists with calls to `put(K, V)`


### Version 1.0.0
Initial version
